require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const axios = require('axios');
const app = express();
const port = 3000;
const user = 'admin';
const pass = '3ckNACpDWpxVbAJH';

// API Settings
mongoose.connect(`mongodb+srv://${user}:${pass}@cluster0-a2p7q.mongodb.net/<dbname>?retryWrites=true&w=majority`,
    {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    }
);
app.use(
    (req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        next();
    }
);
app.use(express.json({ limit: '50mb' }));
app.listen(port, () => console.log(`App listening at http://localhost:${port}`));

// API Controller endpoint: STORE
const Store = require('./models/stores')
const GoogleMapService = require('./services/googleMapsService');
const googleMapService = new GoogleMapService();

app.delete('/store',
    (req, res) => {
        Store.deleteMany({},
            err => {
                res.status(500).send(err)
            }
        );
    }
);

app.post('/store',
    (req, res) => {
        let dbStores = [];
        let stores = req.body;
        stores.forEach(
            store => {
                dbStores.push(
                    {
                        storeName: store.name,
                        phoneNumber: store.phoneNumber,
                        address: store.address,
                        openStatusText: store.openStatusText,
                        addressLines: store.addressLines,
                        location: {
                            type: 'Point',
                            coordinates: [
                                store.coordinates.longitude,
                                store.coordinates.latitude
                            ]
                        }
                    }
                );
            }
        );

        Store.create(dbStores,
            (err, stores) => {
                if (err) {
                    res.status(500).send(err);
                } else {
                    res.status(200).send(stores);
                }
            }
        );
    }
);

app.get('/store',
    (req, res) => {
        const zipCode = req.query.zip_code;
        googleMapService.getCoordinates(zipCode)
            .then(
                coordinates => {
                    Store.find(
                        {
                            location: {
                                $near: {
                                    $maxDistance: 5000, // 5km
                                    $geometry: {
                                        type: 'Point',
                                        coordinates: coordinates
                                    }
                                }
                            }
                        },
                        (err, stores) => {
                            if (err) {
                                res.status(500).send(err);
                            } else {
                                res.status(200).send(stores);
                            }
                        }
                    );
                }
            ).catch(
                error => {
                    console.log(error);
                }
            )
    }
);