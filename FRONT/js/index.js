var map;
var markers = [];
var infoWindow;
const parisCoord = {
    lat: 48.8566,
    lng: 2.3522
};
const losAngelesCoord = {
    lat: 34.0522,
    lng: -118.2437
};
const mapOptions = {
    center: losAngelesCoord,
    zoom: 11,
    mapTypeId: 'roadmap',
    mapTypeControl: false,
    fullscreenControl: false,
    streetViewControl: false,
    styles: [
        { elementType: 'geometry', stylers: [{ color: '#ebe3cd' }] },
        { elementType: 'labels.text.fill', stylers: [{ color: '#523735' }] },
        { elementType: 'labels.text.stroke', stylers: [{ color: '#f5f1e6' }] },
        {
            featureType: 'administrative',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#c9b2a6' }]
        },
        {
            featureType: 'administrative.land_parcel',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#dcd2be' }]
        },
        {
            featureType: 'administrative.land_parcel',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#ae9e90' }]
        },
        {
            featureType: 'landscape.natural',
            elementType: 'geometry',
            stylers: [{ color: '#dfd2ae' }]
        },
        {
            featureType: 'poi',
            elementType: 'geometry',
            stylers: [{ color: '#dfd2ae' }]
        },
        {
            featureType: 'poi',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#93817c' }]
        },
        {
            featureType: 'poi.park',
            elementType: 'geometry.fill',
            stylers: [{ color: '#a5b076' }]
        },
        {
            featureType: 'poi.park',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#447530' }]
        },
        {
            featureType: 'road',
            elementType: 'geometry',
            stylers: [{ color: '#f5f1e6' }]
        },
        {
            featureType: 'road.arterial',
            elementType: 'geometry',
            stylers: [{ color: '#fdfcf8' }]
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry',
            stylers: [{ color: '#f8c967' }]
        },
        {
            featureType: 'road.highway',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#e9bc62' }]
        },
        {
            featureType: 'road.highway.controlled_access',
            elementType: 'geometry',
            stylers: [{ color: '#e98d58' }]
        },
        {
            featureType: 'road.highway.controlled_access',
            elementType: 'geometry.stroke',
            stylers: [{ color: '#db8555' }]
        },
        {
            featureType: 'road.local',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#806b63' }]
        },
        {
            featureType: 'transit.line',
            elementType: 'geometry',
            stylers: [{ color: '#dfd2ae' }]
        },
        {
            featureType: 'transit.line',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#8f7d77' }]
        },
        {
            featureType: 'transit.line',
            elementType: 'labels.text.stroke',
            stylers: [{ color: '#ebe3cd' }]
        },
        {
            featureType: 'transit.station',
            elementType: 'geometry',
            stylers: [{ color: '#dfd2ae' }]
        },
        {
            featureType: 'water',
            elementType: 'geometry.fill',
            stylers: [{ color: '#b9d3c2' }]
        },
        {
            featureType: 'water',
            elementType: 'labels.text.fill',
            stylers: [{ color: '#92998d' }]
        }
    ]
};

initMap = () => {
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    infoWindow = new google.maps.InfoWindow();
}

const searchLocationNear = (stores) => {
    let bounds = new google.maps.LatLngBounds();
    let storesHtml = '';

    stores.forEach(
        (store, index) => {
            let name = store.storeName;
            let address = store.addressLines[0];
            let latLng = new google.maps.LatLng(
                store.location.coordinates[1],
                store.location.coordinates[0]
            );
            let phoneNumber = store.phoneNumber;
            let openStatusText = store.openStatusText;
            let storeNumber = index + 1;
            let fullAddress = `${address} ${store.addressLines[1]}`;
            bounds.extend(latLng);
            storesHtml += `
                <div class="store-container">
                    <div class="store-container-background">
                        <div class="store-info">
                            <div class="store-address">
                                <span>${address}</span>
                                <span>${store.addressLines[1]}</span>
                            </div>
                            <div class="store-phone">${phoneNumber}</div>
                        </div>
                        <div class="store-number-container">
                            <div class="store-number">${storeNumber}</div>
                        </div>
                    </div>
                </div>
            `;
            createMarker(name, address, fullAddress, phoneNumber, latLng, openStatusText, storeNumber);
        }
    );
    map.fitBounds(bounds);
    displayStoresInList(storesHtml);
    setOnClickListener();
}

const noStoresFound = () => {
    var html = `
        <div class="no-stores-found">
            No Stores Found
        </div>
    `;

    displayStoresInList(html);
}

const createMarker = (name, address, fullAddress, phoneNumber, latLng, openStatusText, storeNumber) => {

    let googleUrl = new URL("https://www.google.com/maps/dir/");
    googleUrl.searchParams.append('api', '1');
    googleUrl.searchParams.append('destination', fullAddress);

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,
        label: `${storeNumber}`
    });

    var html = `
        <div class="store-info-window">
            <div class="store-info-name">
                ${name}
            </div>
            <div class="store-info-open-status">
                ${openStatusText}
            </div>
            <div class="store-info-address">
                <div class="icon">
                    <i class="fas fa-location-arrow"></i>
                </div>
                <span>
                <a target="_blank" href="${googleUrl.href}">${address}</a>
                </span>
            </div>
            <div class="store-info-phone">
                <div class="icon">
                    <i class="fas fa-phone-alt"></i>
                </div>
                <span>
                    <a href="tel:${phoneNumber}">${phoneNumber}</a>
                </span>
            </div>
        </div>
    `;

    google.maps.event.addListener(
        marker, 'click',
        () => {
            infoWindow.setContent(html);
            infoWindow.open(map, marker);
        }
    );

    markers.push(marker);
}

const displayStoresInList = (storesHtml) => {
    document.querySelector('.store-list').innerHTML = storesHtml;
}

const setOnClickListener = () => {
    let storeElements = document.querySelectorAll('.store-container');
    storeElements.forEach(
        (storeElement, index) => {
            storeElement.addEventListener('click', () => {
                google.maps.event.trigger(markers[index], 'click');
            });
        }
    )
}

const clearLocations = () => {
    infoWindow.close();
    for (let i = 0; i < markers.length; i++) {
        markers[i].setMap(null);

    }
    markers.length = 0;
}

const onEnter = (event) => {
    if (event.key == "Enter") {
        getInputData();
    }
}

const getInputData = () => {
    const zipCode = document.getElementById('search-input').value;
    if (!zipCode) {
        return;
    }
    clearLocations();
    getStores(zipCode);
}

const getStores = (zipCode) => {
    const API_URL = 'http://localhost:3000/store';
    fetch(`${API_URL}?zip_code=${zipCode}`)
        .then(
            res => {
                if (res.status == 200) {
                    return res.json();
                } else {
                    throw new Error(res.status);
                }
            }
        ).then(
            data => {
                if (data.length > 0) {
                    searchLocationNear(data);
                } else {
                    noStoresFound();
                }
            }
        );
}